import ChomikBox.Exceptions
import requests
import lxml.etree as ET
from multiprocessing import Process, Queue
from requests_toolbelt.multipart.encoder import MultipartEncoder

def UploadThread(filepath, filename, session, hamsterid, folderid, key, stamp, server, locale, position, queue):
    file = open(filepath, 'rb')
    file.seek(position)
    
    fields = [
        ('chomik_id', str(hamsterid)),
        ('folder_id', str(folderid)),
        ('key', key),
        ('time', stamp),
        ('client', 'ChomikBox-2.0.8'),
        ('locale', locale)
    ]
    
    if position > 0:
        fields.append(('resume_from', str(position)))
        
    fields.append(('file', (filename, open(filepath, 'rb'))))
    
    data = MultipartEncoder(fields = fields)
        
    response = session.post('http://{0}/file/'.format(server), data = data, headers = {'Content-Type': data.content_type})
    
    queue.put((response.status_code, response.text))

class Upload:
    __slots__ = ('_filepath', '_filename', '_session', '_hamsterid', '_folderid', '_key', '_stamp', '_server', '_locale', '_stripnamespaces', '_position', '_process', 'response', 'globalid', '_queue')
    
    def __init__(self, filepath, filename, session, hamsterid, folderid, key, stamp, server, locale, stripnamespaces):
        self._filepath = filepath
        self._filename = filename
        self._session = session
        self._hamsterid = hamsterid
        self._folderid = folderid
        self._key = key
        self._stamp = stamp
        self._server = server
        self._locale = locale
        self._stripnamespaces = stripnamespaces
        
        self._position = 0
        self.globalid = None
        
        self._start()
        
    def _start(self):
        self._queue = Queue()
        self._process = Process(target = UploadThread, args = (self._filepath, self._filename, self._session, self._hamsterid, self._folderid, self._key, self._stamp, self._server, self._locale, self._position, self._queue))
        self._process.start()
        
    def wait(self):
        response = self._queue.get()
        # self._process.join()
        
        self.response = response[1]
        
        if response[0] != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response[0])
            
        response = response[1]
            
        root = ET.fromstring(response.replace(' encoding="utf-8"', ''))
        
        result = root.xpath('/resp')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        result = result[0]
            
        code = int(result.get('res'))
        
        if code != 1:
            print(response)
            raise ChomikBox.Exceptions.UploadError
        
        self.globalid = int(result.get('fileid'))
        
    def pause(self):
        self._process.terminate()
        self._process = None
        self._queue = None
        
    def resume(self):
        response = self._session.get('http://{0}/resume/check/'.format(self._server), params = {'key': self._key})
        self.response = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = ET.fromstring(response.text.replace(' encoding="utf-8"', ''))
        
        result = root.xpath('/resp')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        result = result[0]
        
        code = int(result.get('res'))
        
        if code == 1:
            self._position = int(result.get('file_size'))
        elif code != -31:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        self._start()