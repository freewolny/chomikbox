import ChomikBox.RequestsLoader, ChomikBox.Exceptions, ChomikBox.Folder, ChomikBox.Constants, ChomikBox.Upload
from hashlib import md5
import lxml.etree as ET
from xml.sax.saxutils import escape
import codecs, requests
import random
from types import SimpleNamespace

class Box:
    __slots__ = ('_username', '_passwordhash', '_hamsterid', '_token', '_requests', '_stripnamespaces', '_session', 'lastresponse', 'tree')
    
    def __init__(self, username, password, flags = ChomikBox.Constants.BOX_PASSWORD_PLAINTEXT):
        self._username = str(username)
        
        if not ChomikBox.Constants.BOX_PASSWORD_MD5 & flags:
            password = md5(password.encode('utf-8')).hexdigest()
        
        self._passwordhash = str(password)
        
        self._token = None
        self._hamsterid = None
        self._requests = ChomikBox.RequestsLoader()
        
        try:
            xsl_file = codecs.open('stripnamespaces.xsl', 'r', 'utf-8')
            xsl = xsl_file.read()
            xsl_file.close()
            
            self._stripnamespaces = ET.XSLT(ET.XML(xsl))
        except OSError:
            raise ChomikBox.Exceptions.XMLNotFound
            
        self._session = requests.Session()
        self.lastresponse = ''
        
    def login(self):
        data = self._requests.login.format(name = escape(self._username), passHash = escape(self._passwordhash))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.login_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
        
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        resultnode = root.xpath('/Envelope/Body/AuthResponse/AuthResult')
        assert len(resultnode) == 1
        resultnode = resultnode[0]
        result = dict((item.tag, item.text) for item in resultnode.iterchildren())
        
        try:
            assert result['status'] == 'Ok' and result['name'] == self._username
            
            self._hamsterid = result['hamsterId']
            self._token = result['token']  
        except AssertionError:
            if result['status'] == 'PermissionError':
                raise ChomikBox.Exceptions.PermissionError
            elif result['name'] != self._username:
                raise ChomikBox.Exceptions.UnexpectedXML
            else:
                raise NotImplementedError
                
    def _getfolders(self, xmlnode, parent, depth, currentdepth = 1):
        if currentdepth == depth:
            return #None
            
        # folders = []
        parent.folders = []
        
        for _folder in xmlnode.iterchildren('FolderInfo'):
            folder = ChomikBox.Folder(self, int(_folder.find('id').text), _folder.find('name').text, parent)
            
            subfolders = _folder.find('folders')
            assert subfolders is not None
            
            self._getfolders(subfolders, folder, depth, currentdepth + 1)
            
            # _subfolders = self._getfolders(folder, subfolders, depth, currentdepth + 1)
            # if _subfolders is not None:
                # folder.folders = _subfolders
            
            # folders.append(folder)
            parent.append(folder)
            
        #return folders
    
    def getfolders(self, parent, depth = 2):
        assert isinstance(parent, ChomikBox.Folder), 'parent has to be a ChomikBox.Folder'
        assert isinstance(depth, int), 'depth has to be a number'
        
        data = self._requests.folderlist.format(token = escape(self._token), hamsterId = escape(self._hamsterid), folderId = parent.id, depth = depth)
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.folderlist_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
        
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        resultnode = root.xpath('/Envelope/Body/FoldersResponse/FoldersResult')
        assert len(resultnode) == 1
        resultnode = resultnode[0]
        result = dict((item.tag, item.text) for item in resultnode.iterchildren())
        
        try:
            assert result['status'] == 'Ok' and result['hamsterId'] == self._hamsterid
            
            _parent = resultnode.find('folder')
            
            assert _parent.find('id').text == str(parent.id) and _parent.find('name').text == parent.name
            
            subfolders = _parent.find('folders')
            
            assert subfolders is not None
            
            self._getfolders(subfolders, parent, depth)
        except AssertionError:
            if result['status'] != 'Ok':
                raise ChomikBox.Exceptions.StatusError
            else:
                raise ChomikBox.Exceptions.UnexpectedXML
            
    def getrootfolder(self, flags = ChomikBox.Constants.BOX_ROOT_WHOLE_TREE):
        root = ChomikBox.Folder(self, 0, self._username, None)
        if ChomikBox.Constants.BOX_ROOT_WHOLE_TREE & flags:
            self.getfolders(root, 0)
        return root
        
    def _getfolderinfo(self, globalid):
        random.seed()
        stamp = random.randint(1, 100000)
        
        data = self._requests.folderinfo.format(token = escape(self._token), stamp = stamp, id = escape(globalid))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.folderinfo_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        responsestamp = result.find('messageSequence').find('stamp').text
        
        if status != 'Ok':
            raise ChomikBox.Exceptions.StatusError
        if responsestamp != str(stamp):
            raise ChomikBox.Exceptions.UnexpectedXML
        
        folderinfo = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult/list/DownloadFolder')
        
        if folderinfo is None:
            raise ChomikBox.Exceptions.FolderNotFound
        if len(folderinfo) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        return folderinfo[0]
        
    def _getfilesfromxml(self, xmlnode, parent):
        files = [
            ChomikBox.File(
                self,
                int(file.find('id').text),
                file.find('name').text,
                int(file.find('size').text),
                parent
            ) for file in xmlnode.iterchildren('FileEntry')
        ]
        
        return files
        
    def getfiles(self, folder, recursive = False):
        assert isinstance(folder, ChomikBox.Folder), 'ChomikBox.Folder expected'
        
        folderinfo = self._getfolderinfo(folder.globalid)
        
        folder.files = self._getfilesfromxml(folderinfo.find('files'), folder)
        
        if recursive:
            for subfolder in folder.folders:
                self.getfiles(subfolder, True)
        
    def getfolderbyglobalid(self, globalid, flags = ChomikBox.Constants.BOX_FOLDER_WITH_FILES | ChomikBox.Constants.BOX_FOLDER_WITH_SUBFOLDERS, rootfolder = None):
        assert isinstance(globalid, str), 'global ID has to be a string'
        assert rootfolder is None or (isinstance(rootfolder, ChomikBox.Folder) and rootfolder.id == 0 and rootfolder.parent is None), 'root folder or None expected'
        
        folderinfo = self._getfolderinfo(globalid)
        
        if ChomikBox.Constants.BOX_FOLDER_WITH_TREE & flags:
            if rootfolder is None:
                rootfolder = self.getrootfolder(ChomikBox.Constants.BOX_ROOT_WHOLE_TREE)
                
            folder = rootfolder.finddescendantbyid(int(folderinfo.find('id').text))
            
            if folder is None:
                raise ChomikBox.Exceptions.AncestorsError
        else:
            folder = ChomikBox.Folder(self, int(folderinfo.find('id').text), folderinfo.find('name').text, False, folderinfo.find('globalId').text)
            
        if ChomikBox.Constants.BOX_FOLDER_WITH_FILES & flags:
            folder.files = self._getfilesfromxml(folderinfo.find('files'), folder)
            
        if ChomikBox.Constants.BOX_FOLDER_WITH_SUBFOLDERS & flags:
            if not ChomikBox.Constants.BOX_FOLDER_WITH_TREE & flags:
                self.getfolders(folder, 0)
                
            for subfolder in folder.folders:
                self.getfiles(folder, True)
        
        return folder
        
    def folderrename(self, folder, new_name):
        assert isinstance(folder, ChomikBox.Folder), 'folder has to be a ChomikBox.Folder'
        assert isinstance(new_name, str), 'new name has to be a string'
        
        invalid = set('\\/:*?"<>|.')
        
        if any((char in invalid) for char in new_name):
            raise ChomikBox.Exceptions.InvalidCharacters
        
        data = self._requests.renamefolder.format(token = escape(self._token), folderid = folder.id, name = escape(new_name))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.renamefolder_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/RenameFolderResponse/RenameFolderResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        errormessage = result.find('errorMessage').text
        
        if status == 'Error' and errormessage == 'NameAlreadyExists':
            raise ChomikBox.Exceptions.NameConflict
        elif status == 'Error' and errormessage == 'InternalError':
            raise ChomikBox.Exceptions.InternalError
        elif status == 'Error' and errormessage == 'SameName':
            pass
        elif status != 'Ok':
            raise ChomikBox.Exceptions.UnexpectedXML
            
        folder.name = new_name
        
    def makefolder(self, name, parent):
        assert isinstance(name, str), 'name has to be a string'
        assert isinstance(parent, ChomikBox.Folder), 'parent has to be a ChomikBox.Folder'
        
        invalid = set('\\/:*?"<>|.')
        
        if any((char in invalid) for char in name):
            raise ChomikBox.Exceptions.InvalidCharacters
            
        data = self._requests.makefolder.format(token = escape(self._token), newfolderid = parent.id, name = escape(name))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.makefolder_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/AddFolderResponse/AddFolderResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        errormessage = result.find('errorMessage').text
        
        if status == 'Error' and errormessage == 'NameExistsAtDestination':
            raise ChomikBox.Exceptions.NameConflict
        elif status == 'Error' and errormessage == 'FolderNotExists':
            raise ChomikBox.Exceptions.FolderNotFound
        elif status != 'Ok':
            raise ChomikBox.Exceptions.UnexpectedXML
            
        folderid = int(result.find('folderId').text)
        
        return ChomikBox.Folder(self, folderid, name, parent)
        
    def removefolder(self, folder, flags = 0):
        assert isinstance(folder, ChomikBox.Folder), 'folder has to be a ChomikBox.Folder'
        
        data = self._requests.removefolder.format(token = escape(self._token), folderid = folder.id, name = (1 if ChomikBox.Constants.FOLDER_FORCE_REMOVE else 0))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.removefolder_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/RemoveFolderResponse/RemoveFolderResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        errormessage = result.find('errorMessage').text
        
        if status == 'NotEmptyOrNotExistsFolder':
            raise ChomikBox.Exceptions.NameConflict
        elif status != 'Ok':
            raise ChomikBox.Exceptions.UnexpectedXML
            
    def getfilebyglobalid(self, globalid):
        assert isinstance(globalid, str), 'global ID has to be a string'
        
        random.seed()
        stamp = random.randint(1, 100000)
        
        data = self._requests.fileinfo.format(token = escape(self._token), stamp = stamp, id = escape(globalid))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.fileinfo_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        responsestamp = result.find('messageSequence').find('stamp').text
        
        if status != 'Ok':
            raise ChomikBox.Exceptions.StatusError
        if responsestamp != str(stamp):
            raise ChomikBox.Exceptions.UnexpectedXML
        
        folderinfo = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult/list/DownloadFolder')
        
        if folderinfo is None:
            raise ChomikBox.Exceptions.FileNotFound
        if len(folderinfo) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        folderinfo = folderinfo[0]
        
        fileinfo = folderinfo.find('files').find('FileEntry')
        
        if fileinfo is None:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        return ChomikBox.File(
            self,
            int(fileinfo.find('id').text),
            fileinfo.find('name').text,
            int(fileinfo.find('size').text),
            folderinfo.find('globalId').text
        )
        
    def geturls(self, files):
        assert isinstance(files, list) and all(isinstance(x, ChomikBox.File) for x in files), 'file has to be a list of ChomikBox.Files'
        
        if not files:
            return []
        
        random.seed()
        stamp = random.randint(1, 100000)
        
        fileentry = '<DownloadReqEntry><id>{.id}</id><agreementInfo><AgreementInfo><name>own</name></AgreementInfo></agreementInfo></DownloadReqEntry>'
        
        data = self._requests.fileurl.format(token = escape(self._token), stamp = stamp, files = fileentry * len(files)).format(*files)
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.fileinfo_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        responsestamp = result.find('messageSequence').find('stamp').text
        
        if status != 'Ok':
            raise ChomikBox.Exceptions.StatusError
        if responsestamp != str(stamp):
            raise ChomikBox.Exceptions.UnexpectedXML
        
        folderinfo = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult/list/DownloadFolder')
        
        if folderinfo is None:
            raise ChomikBox.Exceptions.FileNotFound
        if len(folderinfo) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        folderinfo = folderinfo[0]
        
        fileentries = root.xpath('/Envelope/Body/DownloadResponse/DownloadResult/list/DownloadFolder/files/FileEntry')
        
        if fileentries is None:
            raise ChomikBox.Exceptions.UnexpectedXML
            
        urls = []
        
        for fileentry in fileentries:
            for i in range(len(urls), len(files)):
                if fileentry.find('id').text == str(files[i].id):
                    urls.append(fileentry.find('url').text)
                    break
                else:
                    urls.append(None)
                    
        return urls
        
    def _geturltreestruct(self, files, folder, depth, currentdepth = 1):
        if currentdepth == depth:
            files += folder.files
            return SimpleNamespace(files = folder.files, folders = {})
        else:
            files += folder.files
            return SimpleNamespace(files = folder.files, folders = {subfolder.name: self._geturltreestruct(files, subfolder, depth, currentdepth + 1) for subfolder in folder.folders})
            
    def _geturltree(self, urls, struct, shift = 0):
        struct.files = [urls[shift + i] for i in range(len(struct.files))]
        shift += len(struct.files)
        struct.folders = {name: self._geturltree(urls, _struct, shift) for name, _struct in struct.folders.items()}
        
        return struct
        
    def geturltree(self, folder, depth = 0):
        assert isinstance(folder, ChomikBox.Folder), 'folder has to be a ChomikBox.Folder'
        
        files = []
        struct = self._geturltreestruct(files, folder, depth)
        
        self._geturltree(self.geturls(files), struct)
        
        return struct
        
    def upload(self, filepath, folder, filename):
        assert isinstance(folder, ChomikBox.Folder), 'folder has to be a ChomikBox.Folder'
        assert isinstance(filename, str), 'file name has to be a string'
        
        data = self._requests.upload.format(token = escape(self._token), folderid = folder.id, filename = escape(filename))
        
        response = self._session.post('http://box.chomikuj.pl/services/ChomikBoxService.svc', data = data, headers = self._requests.upload_headers)
        self.lastresponse = response
        
        if response.status_code != requests.codes.ok:
            raise ChomikBox.Exceptions.HTTPError(response.status_code)
            
        root = self._stripnamespaces(ET.fromstring(response.text))
        
        result = root.xpath('/Envelope/Body/UploadTokenResponse/UploadTokenResult')
        
        if result is None or len(result) != 1:
            raise ChomikBox.Exceptions.UnexpectedXML
        
        result = result[0]
        
        status = result.find('status').text
        errormessage = result.find('errorMessage').text
        
        if status == 'ValidationError' and errormessage == 'File name is not valid : HasInvalidCharacters':
            raise ChomikBox.Exceptions.InvalidCharacters
        elif status != 'Ok':
            raise ChomikBox.Exceptions.UnexpectedXML
            
        return ChomikBox.Upload(filepath, filename, self._session, self._hamsterid, folder.id, result.find('key').text, result.find('stamp').text, result.find('server').text, result.find('locale').text, self._stripnamespaces)