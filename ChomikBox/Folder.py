import ChomikBox.Box, ChomikBox.File, ChomikBox.Exceptions, ChomikBox.Constants
from urllib.parse import quote_plus

class Folder:
    __slots__ = ('box', 'id', 'name', '_parent', '_globalid', '_folders', '_files')
    
    def __init__(self, box, id, name, parent = None, globalid = None, folders = None, files = None):
        assert isinstance(box, ChomikBox.Box), 'Box expected'
        assert isinstance(id, int), 'ID has to be a number'
        assert isinstance(name, str), 'name has to be a string'
        assert isinstance(parent, Folder) or parent is None or parent is False, 'parent has to be a Folder, None or False'
        assert isinstance(globalid, str) or globalid is None, 'globalid has to be a string or None'
        assert parent is not False or globalid is not None, 'either parent or globalid has to be specified'
        assert (isinstance(folders, list) and all(isinstance(x, Folder) for x in folders)) or folders is None, 'folders have to be a list of Folders or None'
        assert (isinstance(files, list) and all(isinstance(x, ChomikBox.File) for x in files)) or files is None, 'files have to be a list of Files or None'
        
        self.box = box
        self.id = id
        self.name = name
        self._parent = parent
        self._globalid = globalid
        self._folders = None
        self._files = None
        
    @property
    def parent(self):
        if self._parent is False:
            self._parent = self.box.getfolderbyglobalid(self.globalid.rsplit('/', 1)[0], ChomikBox.Constants.BOX_FOLDER_WITH_TREE)
            
        return self._parent
        
    @property
    def globalid(self):
        if self._globalid is None:
            self._globalid = ('' if self.parent == None else self.parent.globalid) + '/' + quote_plus(self.name, '!()')
            
        return self._globalid
    
    @property
    def folders(self):
        if self._folders is None:
            self.box.getfolders(self)
        
        return self._folders
        
    @folders.setter
    def folders(self, _list):
        assert isinstance(_list, list)
        
        self._folders = []
        
        for item in _list:
            if not isinstance(item, Folder):
                raise ChomikBox.Exceptions.FolderAppendType
                
            self._folders.append(item)
        
    @property
    def files(self):
        if self._files is None:
            self.box.getfiles(self)
            
        return self._files
        
    @files.setter
    def files(self, _list):
        assert isinstance(_list, list)
        
        self._files = []
        
        for item in _list:
            if not isinstance(item, ChomikBox.File):
                raise ChomikBox.Exceptions.FolderAppendType
                
            self._files.append(item)
    
    def _appendfolder(self, folder):
        if self._folders is None:
            self._folders = []
        
        self._folders.append(folder)
    
    def _appendfile(self, file):
        if self._files is None:
            self._files = []
        
        self._files.append(file)
    
    def append(self, child):
        if isinstance(child, Folder):
            self._appendfolder(child)
        elif isinstance(child, ChomikBox.File):
            self._appendfile(file)
        else:
            raise ChomikBox.Exceptions.FolderAppendType
    
    def __iter__(self):
        for folder in self.folders:
            yield folder
        
        for file in self.files:
            yield file
            
    def finddescendantbyid(self, id):
        assert isinstance(id, int), 'ID has to be a number'
        
        if self.id == id:
            return self
        elif not self.folders:
            return None
            
        for folder in self.folders:
            descendant = folder.finddescendantbyid(id)
            
            if descendant is not None:
                return descendant
                
    def rename(self, new_name):
        self.box.folderrename(self, new_name)