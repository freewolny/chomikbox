import codecs, ChomikBox.Exceptions

class RequestsLoader:
    __slots__ = ('_directory', 'login', 'login_headers', 'folderlist', 'folderlist_headers', 'folderinfo', 'folderinfo_headers', 'renamefolder', 'renamefolder_headers', 'makefolder', 'makefolder_headers', 'removefolder', 'removefolder_headers', 'fileinfo', 'fileinfo_headers', 'fileurl', 'fileurl_headers', 'upload', 'upload_headers')
    
    _directory = 'requests/'
    
    def __getattr__(self, name):
        if name not in self.__slots__:
            raise ChomikBox.Exceptions.RequestNotFound
            
        _name = name.split('_headers')[0]
        
        try:
            with codecs.open(self._directory + _name + '.xml', 'r', 'utf-8') as xml_file, codecs.open(self._directory + _name + '_headers.txt', 'r', 'utf-8') as headers_file:
                xml = xml_file.read()
                xml = xml.replace('\t', '').replace('\r', '').replace('\n', '')
                setattr(self, _name, xml)
                
                headers = {}
                
                try:
                    for line in headers_file:
                        header_line = line.strip().split(': ', 1)
                        headers[header_line[0]] = header_line[1]
                except IndexError:
                    raise ChomikBox.Exceptions.HeadersSyntax
                
                setattr(self, _name + '_headers', headers)
        except OSError:
            raise ChomikBox.Exceptions.XMLNotFound
        else:
            return None
        finally:
            return getattr(self, name)