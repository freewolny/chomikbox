class Base(Exception):
    pass
    
class XMLNotFound(Base):
    pass
    
class HeadersNotFound(Base):
    pass
    
class HeadersSyntax(Base):
    pass
    
class RequestNotFound(Base):
    pass
    
class PermissionError(Base):
    pass
    
class UnexpectedXML(Base):
    pass
    
class FolderAppendType(Base):
    pass
    
class HTTPError(Base):
    pass
    
class StatusError(Base):
    pass
    
class FolderNotFound(Base):
    pass
    
class AncestorsError(Base):
    pass
    
class InvalidCharacters(Base):
    pass
    
class NameConflict(Base):
    pass
    
class FolderRemoveError(Base):
    pass
    
class InternalError(Base):
    pass
    
class FileNotFound(Base):
    pass
    
class UploadError(Base):
    pass