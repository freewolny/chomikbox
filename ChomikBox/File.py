import ChomikBox.Box, ChomikBox.Folder

class File:
    __slots__ = ('self', 'box', 'id', 'name', 'size', '_parent')
    
    def __init__(self, box, id, name, size, parent):
        assert isinstance(box, ChomikBox.Box), 'Box expected'
        assert isinstance(id, int), 'ID has to be a number'
        assert isinstance(name, str), 'name has to be a string'
        assert isinstance(size, int), 'size has to be a number'
        assert isinstance(parent, (ChomikBox.Folder, str)), 'parent has to be a ChomikBox.Folder or its global ID'
        
        self.box = box
        self.id = id
        self.name = name
        self.size = size
        self._parent = parent
        
    @property
    def parent(self):
        if isinstance(self._parent, str):
            self._parent = self.box.getfolderbyglobalid(self._parent, ChomikBox.Constants.BOX_FOLDER_WITH_TREE)

        return self._parent
        
    def geturl(self):
        return self.box.geturls([self])
        
    